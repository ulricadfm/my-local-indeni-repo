package com.indeni.server.rules.library.templatebased.f5

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

/**
  *
  */
case class f5_service_check(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "f5_service_check",
  ruleFriendlyName = "F5 Devices: Service check approaching",
  ruleDescription = "indeni will alert several weeks before a service check occurs on F5 devices.",
  metricName = "f5-service-check",
  alertDescription = "The service check for this device has passed. Upgrades post the service check date may fail. For more information see https://support.f5.com/csp/article/K7727\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  baseRemediationText = "Contact F5 support.")()
