package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}
import com.indeni.time.TimeSpan

/**
  *
  */
case class all_devices_ntp_not_syncing(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "all_devices_ntp_not_syncing",
  howManyRepetitions = 2,
  ruleFriendlyName = "All Devices: NTP sync failure(s)",
  ruleDescription = "Indeni will alert if one or more of the configured NTP servers is not syncing correctly.",
  metricName = "ntp-server-state",
  applicableMetricTag = "name",
  alertItemsHeader = "NTP Servers Affected",
  alertDescription = "One or more NTP servers configured on this device is not responding.",
  baseRemediationText = "Review the cause for the NTP sync not working.")(
  ConditionalRemediationSteps.VENDOR_CP -> "Review sk92602: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk92602",
  ConditionalRemediationSteps.VENDOR_PANOS -> "Run \"show ntp\" and review the status of each NTP server. You can also review the dagger.log, based on https://live.paloaltonetworks.com/t5/tkb/articleprintpage/tkb-id/Management-TKB/article-id/2078",
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |Examples of common NTP issues are the next:
      |• NTP packets are not received.
      |• NTP packets are received, but are not processed by the NTP process on the NX-OS.
      |• NTP packets are processed, but erroneous factors or packet data causes the loss of synchronization.
      |• NTP clock-period is manually set.
      |
      |1. Check the current ntp status by running the NX-OS command "show ntp peer-status".
      |2. If the "show ntp peer-status" command does not provide any output then try to ping the NTP servers. The ntp source and vrf may need to be provided as command options.
      |3. Check the routing table with the "show ip route vrf all" NX-OS command to verify that there is routing to the NTP servers.
      |4. Check that the UDP 123 port used by NTP service is permitted to the network.
      |5. Execute the "show run ntp" NX-OS command to review the NTP current configuration.
      |6. For more information review the next Nexus NTP troubleshooting guide: https://www.cisco.com/c/en/us/support/docs/ip/network-time-protocol-ntp/116161-trouble-ntp-00.html""".stripMargin
)
