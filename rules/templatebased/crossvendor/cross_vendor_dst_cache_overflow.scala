package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NearingCapacityTemplateRule}

/**
  *
  */
case class cross_vendor_dst_cache_overflow(context: RuleContext) extends NearingCapacityTemplateRule(context,
  ruleName = "cross_vendor_dst_cache_overflow",
  ruleFriendlyName = "All Devices: High destination cache usage",
  ruleDescription = "indeni will alert when the number of entries stored in a device's destination cache is nearing the allowed limit.",
  usageMetricName = "destination-cache-usage",
  limitMetricName = "destination-cache-limit",
  threshold = 80.0,
  alertDescriptionFormat = "The destination cache table has %.0f entries where the limit is %.0f. This table is used to cache routing decisions and increase the speed of traffic forwarding.",
  baseRemediationText = "Identify the cause of the large destination cache. If it is due to a legitimate cause, such as a high number of hosts visible on the available networks, please contact your technical support provider.")(
  ConditionalRemediationSteps.VENDOR_CP -> "Review sk74480: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk74480")
