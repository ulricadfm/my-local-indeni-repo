package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule

/**
  *
  */
case class checkpoint_compare_securexl_setting_vsx(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "checkpoint_compare_securexl_setting_vsx",
  ruleFriendlyName = "Check Point Cluster (VSX): SecureXL configuration mismatch across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the SecureXL settings are different for different VS's.",
  metricName = "securexl-status",
  applicableMetricTag = "vs.name",
  isArray = false,
  alertDescription = "The members of a cluster of Check Point firewalls must have the same SecureXL settings.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/pub/gal-vitenberg/83/484/103\">Gal Vitenberg</a>.",
  baseRemediationText = """Compare the output of "fwaccel stat" across members of the cluster, make sure to run the command in the correct vsenv context.""")()
