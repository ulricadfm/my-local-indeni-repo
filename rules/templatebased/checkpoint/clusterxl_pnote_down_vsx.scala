package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

/**
  *
  */
case class clusterxl_pnote_down_vsx(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "clusterxl_pnote_down_vsx",
  ruleFriendlyName = "Check Point ClusterXL (VSX): Pnote(s) down",
  ruleDescription = "ClusterXL has multiple problem notifications (pnotes) - if any of them fail an alert will be issued.",
  metricName = "clusterxl-pnote-state",
  applicableMetricTag = "name",
  descriptionMetricTag = "vs.name",
  alertItemsHeader = "Problematic Elements",
  alertDescription = "Some VS's in this cluster member are down due to certain elements being in a \"problem state\".\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/pub/gal-vitenberg/83/484/103\">Gal Vitenberg</a>.",
  baseRemediationText = "Review the list of problematic elements and take appropriate action.",
  itemSpecificDescription = Seq (
    "(?i).*FIB.*".r -> "The FIB device is responsible for supporting dynamic routing under ClusterXL. Review the firewall logs to ensure traffic with the FIBMGR service is flowing correctly.",

    // Catch-all
    ".*".r -> "Please consult with your technical support provider about this pnote."
  ),
  // Ignore interface active check, we alert about interface count separately
  itemsToIgnore = Set ("Interface Active Check".r))()
