package com.indeni.server.rules.library.checkpoint

import com.indeni.data.Snapshot._
import com.indeni.data.conditions.True
import com.indeni.ruleengine.expressions.conditions.{And, Equals, Not}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.utility.IsEmptyExpression.IsEmptyExpressionHelper
import com.indeni.server.rules._
import com.indeni.server.rules.library.{ConditionalRemediationSteps, PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

import scala.language.reflectiveCalls

case class StaticArpEmptyRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("chkp_firewall_static_arp_empty", "Check Point Firewalls: Static ARP table lost",
    "Check Point firewalls have a static ARP table. Sometimes it may lose its contents, for various reasons. If it does, indeni will alert.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val currentValue = SnapshotExpression("static-arp").asMulti().mostRecent().noneable
    val previousValue = SnapshotExpression("static-arp").asMulti().middle()

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
      StatusTreeExpression(
        // The time-series we check the test condition against:
        SelectSnapshotsExpression(context.snapshotsDao, Set("static-arp")).multi(),

        // The condition which, if true, we have an issue. Checked against the time-series we've collected
        And(
          previousValue.nonEmpty,
          Not(Equals(currentValue, previousValue)),
          Equals(currentValue, ConstantExpression(Some(Seq[SingleDimension]()))))
      ).withoutInfo().asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("The static ARP table which was configured on this Check Point firewall is now empty."),
        ConditionalRemediationSteps("Review sk98740: https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk98740"
        )
    )
  }
}


