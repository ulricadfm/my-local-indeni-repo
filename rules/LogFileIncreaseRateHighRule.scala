package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.UpSlopeDetection
import com.indeni.ruleengine.expressions.conditions.ResultsFound
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data._
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

case class LogFileIncreaseRateHighRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("chkp_fw_log_increase_rate_high", "Check Point Firewalls: Firewall logging locally",
    "indeni will track the size of the fw.log file to detect when the firewall is logging locally.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("log-file-size")

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectTimeSeriesExpression[Double](context.tsDao, Set("log-file-size"), historyLength = ConstantExpression(TimeSpan.fromHours(1))),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          ResultsFound(UpSlopeDetection(actualValue))
        ).withoutInfo().asCondition()
    ).withRootInfo(
        getHeadline(),
        scopableStringFormatExpression("The $FWDIR/log/fw.log file is growing. This indicates local logging."),
        ConditionalRemediationSteps("Determine why this is occurring - it could potentially be a connectivity issue to the log server, or too many logs being generated per second.",
          ConditionalRemediationSteps.VENDOR_CP -> "Review https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk40090"
        )
    )
  }
}
