 #! META
name: f5-tmsh-list-ltm-profile-tcp-idle-timeout-list-ltm-virtual-profiles
description: Find use of tcp profiles with too high timeout
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    shell: "bash"

#! COMMENTS
f5-virtualserver-tcp-profile-idle-timeout:
    why: |
        Having very long tcp idle timeouts for virtual servers could make the load balancer keep too many connections open, which in turn could potentially cause memory exhaustion.
    how: |
        This alert logs into the F5 through SSH and retrieves a list of tcp profiles and virtual servers and finds if any tcp profiles with long timeouts has been used.
    without-indeni: |
        Log into the device through SSH. Enter TMSH and issue the command "list ltm profile tcp idle-timeout;list ltm virtual profiles". Look through each tcp profile definition for the use idle timeouts equal to, or over 1800 seconds and then match that to the profile use of each virtual server.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
tmsh -q -c "list ltm profile tcp idle-timeout;list ltm virtual profiles"

#! PARSER::AWK

#ltm profile tcp mptcp-mobile-optimized {
/^ltm profile tcp/{

    section = "tcpProfile"
    tcpProfileName = $4

}

#    idle-timeout 300
/^\s+idle-timeout\s[0-9]+$/{

    idleTimeout = $2
    tcpProfileIdleTimeouts[tcpProfileName] = idleTimeout

}

#ltm virtual compression-level-6-vip-443 {
/^ltm virtual/{

    section = "virtualserver"
    virtualName = $3

}

#        wam-tcp-lan-optimized {
/^\s+[^\s]+\s\{$/{

    #Filter out only the profile names
    if(section == "virtualserver" && $1 in tcpProfileIdleTimeouts){
        virtualTags["name"] = "Virtual Server: " virtualName " TCP Profile: " $1
        writeComplexMetricString("f5-virtualserver-tcp-profile-idle-timeout", virtualTags, tcpProfileIdleTimeouts[$1])
    }

}
