#! META
name: f5-rest-mgmt-cm-system-machineid-resolver-state
description: Determine the state of the added devices in BIG-IQ
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: "f5"
    product: "big-iq"
    rest-api: "true"
    shell: "bash"

#! COMMENTS
trust-connection-state:
    why: |
        If a device is considered as down from BIG-IQ, then monitoring and automation targeting the device would be impacted. 
    how: |
        This script uses the F5 iControl REST API to retrieve the current list of the configured devices and their states.
    without-indeni: |
        The device state is available by logging into the BIG-IQ Web Interface, choosing the Device Management module and then "BIG-IP DEVICES".
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::HTTP
url: /mgmt/cm/system/machineid-resolver
protocol: HTTPS

#! PARSER::JSON
_dynamic_vars:
    _dynamic:
        "machineId":
            _value: "$.items[0:].machineId"

#! REMOTE::HTTP
url: /mgmt/cm/system/machineid-resolver/${machineId}/stats
protocol: HTTPS
            
#! PARSER::JSON
_metrics:
    -
        _tags:
            "im.name":
                _constant: "trust-connection-state"
            "name":
                _value: "$.entries.itemId.description"
        _temp:
            "health":
                _value: "$.entries.['health.summary.available'].value"
        _transform:
            _value.double: |
                {
                    if("${temp.health}" == 1){
                        print "1"
                    } else {
                        print "0"
                    }
                }
                
