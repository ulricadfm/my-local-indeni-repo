#! META
name: f5-tmsh-show-vcmp-global-show-vcmp-virtual-disk
description: Extract the vCMP host disk metrics
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    vsx: "true"
    shell: "bash"

#! COMMENTS
f5-vcmp-guest-used-disk-kbytes:
    why: |
        Disk used per guest could be useful to see in order to get an overview of a vCMP host and its disk usage.
    how: |
        This script logs into the device through SSH and extracts the disk used by guests via TMSH.
    without-indeni: |
        An administrator could track this manually by logging into the web interface, clicking on "vCMP" -> "Virtual Disk List".
    can-with-snmp: true
    can-with-syslog: false
f5-vcmp-host-disk-usage-percentage:
    why: |
        Running out of disk space on the vCMP host would mean that no new guests can be created. This alert would notify administrators of low disk space so they can prepare to clear some in case they need to deploy new guests.
    how: |
        This script logs into the device through SSH and extracts the host disk usage via TMSH.
    without-indeni: |
        An administrator could track this manually by logging into the web interface, clicking on "vCMP" -> "Virtual Disk List". On this page the available disk space and the disk used by the guests is available. This information is also available by logging into the device through SSH, entering TMSH and executing the command "show vcmp global", followed by "show vcmp virtual-disk".
    can-with-snmp: true
    can-with-syslog: false
f5-vcmp-host-disk-used-kbytes:
    skip-documentation: true
f5-vcmp-host-disk-total-kbytes:
    skip-documentation: true
        

#! REMOTE::SSH
tmsh -q -c "show vcmp global kil; show vcmp virtual-disk kil"

#! PARSER::AWK

#Vcmp::Global
/^Vcmp::Global$/{
    section = "global"
}

#Vcmp::VirtualDisk
/^Vcmp::VirtualDisk$/{
    section="virtualDisk"
}

#262601088.0
/^[\d\.]+$/{
    if(section == "global"){
        totalFreeDiskSpace = $1
    }
}

#mylb1.domain.local.img                tmos  in-use        12962156.0
/^.+[\d\.]+$/{
    if(section == "virtualDisk"){
    
        guestsArray[$1] = $NF
        
        allocatedDiskTags["name"] = $1
        
        #Write metric with disk space in kilobytes as value
        writeDoubleMetricWithLiveConfig("f5-vcmp-guest-used-disk-kbytes", allocatedDiskTags, "gauge", 3600, $NF, "vCMP guest disk use", "kilobytes", "name")
    
    }
}

END {
    
    totalDiskSpace = totalFreeDiskSpace
    
    #Add used disk space to get the total available disk space
    for(guest in guestsArray){
        totalDiskSpace += guestsArray[guest]
    }
    
    usedDiskSpace = totalDiskSpace - totalFreeDiskSpace
    
    diskUsage = usedDiskSpace/(totalDiskSpace)*100

    writeDoubleMetric("f5-vcmp-host-disk-used-kbytes", null, "gauge", 3600, usedDiskSpace)
    writeDoubleMetric("f5-vcmp-host-disk-total-kbytes", null, "gauge", 3600, totalDiskSpace)
    writeDoubleMetricWithLiveConfig("f5-vcmp-host-disk-usage-percentage", null, "gauge", 3600, diskUsage, "vCMP host disk usage", "percentage")

}


