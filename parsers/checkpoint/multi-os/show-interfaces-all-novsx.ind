#! META
name: chkp-os-interfaces
description: get interface information
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: checkpoint
    and:
        -
            or:
                -
                    os.name: gaia
                -
                    os.name: secureplatform
        -
            or:
                -
                    vsx: 
                        neq: true
                -
                    mds: true

#! COMMENTS
network-interface-state:
    why: |
        Interfaces in the "down" state could result in downtime or reduced redundancy.
    how: |
        The state of the interface is retrieved by running "ethtool".
    without-indeni: |
        An administrator could login and manually check interface status, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or WebUI.

network-interface-admin-state:
    why: |
        If the interface is disabled, then it is okay for it to be down. If the interface is enabled however, it should be up.
    how: |
        Retrieve the information by parsing the Gaia database in /config/active.
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or WebUI.

network-interface-speed:
    why: |
        If the interface speed is set to a low value, this could mean auto-negotiation is not working correctly and the interface does not utilize the full bandwidth available.
    how: |
        The speed of the interface is retrieved by running "ethtool".
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or WebUI.

network-interface-duplex:
    why: |
        If the interface has half-duplex setting, this will reduce throughput, and should be investigated.
    how: |
        The duplex of the interface is retrieved by running "ethtool".
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or WebUI.

network-interface-ipv4-address:
    why: |
        To be able to search for IP addresses in indeni, this data needs to be stored.
    how: |
        The IP address of the interface is retrieved by running "ifconfig -a".
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP, WebUI or SmartDashboard.

network-interface-ipv4-subnet:
    why: |
        To be able to search for IP addresses in indeni, this data needs to be stored.
    how: |
        The subnet of the interface is retrieved by running "ifconfig -a".
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP, WebUI or SmartDashboard.

network-interface-type:
    why: |
        The type of interface can be useful for administrators.
    how: |
        The type of the interface is retrieved by running "ifconfig -a".
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface or SNMP.

network-interface-mtu:
    why: |
        The MTU sometimes needs to be adjusted. Storing this gives an administrator an easy way to view the MTU from a large number of devices, as well as identifying incorrectly set MTU.
    how: |
        The MTU of the interface is retrieved by running "ifconfig -a".
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or WebUI.

network-interface-mac:
    why: |
        To be able to search for MAC addresses in indeni, this data needs to be stored.
    how: |
        The MAC address of the interface is retrieved by running "ifconfig -a".
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or WebUI.

network-interface-description:
    why: |
        The description is an important way to identify interfaces.
    how: |
        Retrive the information by parsing the gaia database in /config/active.
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or WebUI.

network-interface-tx-bytes:
    why: |
        It is useful to know how much data has been transmitted by the interface.
    how: |
        How many bytes sent by the interface is retrieved by running "ifconfig -a".
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or WebUI.

network-interface-rx-bytes:
    why: |
        It is useful to know how much data has been received by the interface.
    how: |
        How many bytes received by the interface is retrieved by running "ifconfig -a".
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or WebUI.

network-interface-tx-packets:
    why: |
        It is useful to know how many packets have been transmitted by the interface.
    how: |
        How many packets sent by the interface is retrieved by running "ifconfig -a".
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or WebUI.

network-interface-rx-packets:
    why: |
        It is useful to know how many packets have been received by the interface.
    how: |
        How many packets received by the interface is retrieved by running "ifconfig -a".
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or WebUI.

network-interface-tx-errors:
    why: |
        Transmit errors on an interface could indicate a problem.
    how: |
        The amount of transmit errors for the interface is retrieved by running "ifconfig -a".
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or WebUI.

network-interface-rx-dropped:
    why: |
        Dropped packets on an interface could indicate a problem and potential traffic loss.
    how: |
        The amount of receive drops for the interface is retrieved by running "ifconfig -a".
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or WebUI.

network-interface-tx-overruns:
    why: |
        Transmit overruns on an interface could indicate a problem.
    how: |
        The amount of transmit overruns for the interface is retrieved by running "ifconfig -a".
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or WebUI.

network-interface-rx-overruns:
    why: |
        Receive overruns on an interface could indicate a problem.
    how: |
        The amount of receive overruns for the interface is retrieved by running "ifconfig -a".
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or WebUI.

network-interface-tx-carrier:
    why: |
        A high carrier number could mean that the link is flapping.
    how: |
        The carrier counter for the interface is retrieved by running "ifconfig -a".
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or WebUI.

network-interface-rx-frame:
    why: |
        A high frame number means a lot of packages did not end on a 32bit/4 byte boundary.
    how: |
        The frame counter for the interface is retrieved by running "ifconfig -a".
    without-indeni: |
        An administrator could login and manually check interface configuration, or use SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or WebUI.

network-interfaces:
    skip-documentation: true

network-interface-:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 ifconfig -a|grep "HWaddr"| awk {'print $1'}| while read interface; do ${nice-path} -n 15 ifconfig $interface && ${nice-path} -n 15 ethtool -i $interface && ${nice-path} -n 15 ethtool $interface ; ${nice-path} -n 15 grep "interface:$interface" /config/active; done ; ifconfig |awk '/^[a-zA-Z]/ { print "enabled: " $1 }'


#! PARSER::AWK

############
# Script explanation: We should avoid running clish commands due to the excessive logs in /var/log/messages that creates. So ifconfig and parsing /config/active instead.
###########

# LAN1.246      Link encap:Ethernet  HWaddr 00:1C:7F:23:26:EB
/Link encap:/ {

    interfaceName = trim($1)
    stattags["name"] = interfaceName
    interfaces[interfaceName, "name"] = interfaceName
	adminStateArr[interfaceName] =  0
	
	# Type
	type = $3
	gsub(/encap:/, "", type)
	writeComplexMetricString("network-interface-type", stattags, type)
	
	# MAC
	writeComplexMetricString("network-interface-mac", stattags, $5)
}


# inet addr:192.168.245.2  Bcast:192.168.245.255  Mask:255.255.255.0
/inet addr:/ {
	ip = $2
	subnet = $4
	
	gsub(/addr:/, "", ip)
	gsub(/Mask:/, "", subnet)
	
	writeComplexMetricStringWithLiveConfig("network-interface-ipv4-address", stattags, ip, "Interfaces IPv4 Address")
	writeComplexMetricStringWithLiveConfig("network-interface-ipv4-subnet", stattags, subnet, "Interfaces IPv4 Subnet")
	
	# make array for interfacename -> IP
	interfaceArr[interfaceName] = ip
}



# BROADCAST MULTICAST  MTU:1500  Metric:1
# UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
/MTU/ {
	# MTU
	mtu = $(NF-1)
	gsub(/MTU:/, "", mtu)
	interfaces[interfaceName, "mtu"] = mtu
	writeComplexMetricString("network-interface-mtu", stattags, mtu)
}

#TX bytes:2096249117 packets:2975595223 errors:0 dropped:0 overruns:0 carrier:0
#RX bytes:3964467449 (3.6 GiB)  TX bytes:922468769 (879.7 MiB)
/X bytes/ {
    if ($1 == "TX") {
        txrxprefix = "tx"
    } else {
        txrxprefix = "rx"
    }

    for (i = 2; i<=NF; i++) {
        if (split($i, statparts, ":") == 2) {
            name = statparts[1]
            value = statparts[2]
            writeDoubleMetricWithLiveConfig("network-interface-" txrxprefix "-" name, stattags, "counter", "60", value, "Network Interfaces - " txrxprefix " " name, "number", "name")
        }
    }
}

# RX packets:33471990 errors:0 dropped:0 overruns:0 frame:0
/X packets/ {
    if ($1 == "TX") {
        txrxprefix="tx"
    } else {
        txrxprefix="rx"
    }

    for (i = 2; i<=NF; i++) {
        if (split($i, statparts, ":") == 2) {
            name = statparts[1]
            value = statparts[2]
            writeDoubleMetricWithLiveConfig("network-interface-" txrxprefix "-" name, stattags, "counter", "60", value, "Network Interfaces - " txrxprefix " " name, "number", "name")
        }
    }
}

# driver: vmxnet3
/driver:/ {
   interfaces[interfaceName, "driver"] = $2
}

# Speed: 100Mb/s
/Speed:/ {
	speed = $2
	gsub(/b\/s/, "", speed)
	writeComplexMetricString("network-interface-speed", stattags, speed)
}

# Duplex: Full
/Duplex:/ {
	if ($2 == "Full") {
		writeComplexMetricString("network-interface-duplex", stattags, "full")
	} else if ($2 == "Half") {
		writeComplexMetricString("network-interface-duplex", stattags, "half")
	}
}


# Link detected: yes
/Link detected:/ {
	if ($3 == "yes") {
		linkstate = 1
	} else {
		linkstate = 0
	}
	writeDoubleMetricWithLiveConfig("network-interface-state", stattags, "gauge", "60", linkstate, "Network Interfaces - Up/Down", "state", "name")
}

# Auto-negotiation: on
/Auto-negotiation:/ {
	interfaces[interfaceName, "auto-negotiation"] = $2
}

# interface:eth2:state on
/interface:[a-z]+[0-9]+:state/ {

	# Admin state
	if ($2 != "off") {
		adminState = 1
	} else {
		adminState = 0
	}
	adminStateArr[interfaceName] = adminState
	
	# If we found data using the parsing of the /config/active file, we do not need to use the standard linux way.
	interfaceStateGaia = 1
}

# interface:eth1:comments Private2\ lala
# interface:eth1:comments Test
/interface:[a-z]+[0-9]+:comments/ {
	# If the comment is in two words or more, a backslash is inserted. We need to remove it
	gsub(/interface:[a-z]+[0-9]+:comments/, "", $0)
	gsub(/(\\)/,"",$0)
	writeComplexMetricString("network-interface-description", stattags, trim($0))
}

# enabled: eth2
/^enabled:/ {
	if (interfaceStateGaia != 1) {
		adminStateArr[$2] = 1
	}
}


END {
	writeComplexMetricObjectArray("network-interfaces", t, interfaces)
	
	for (id in adminStateArr) {
		stattags["name"] = id
		adminState = adminStateArr[id]
		writeDoubleMetricWithLiveConfig("network-interface-admin-state", stattags, "gauge", "60", adminState, "Network Interfaces - Enabled/Disabed", "state", "name")
	}
}
