#! META
name: chkp-gaia-clish_show_pbr_rules
description: run "show pbr rules" over clish
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: checkpoint
    os.name: gaia

#! COMMENTS
pbr-rules:
    why: |
        It is important that the routing is configured the same for all cluster members of the same cluster. Otherwise there can be downtime in the event of a failover.
    how: |
        By parsing the gaia configuration database, /config/active, the PBR settings are retrieved. It can also be retrieved via clish, but that creates a lot of log entries in /var/log/messages.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing policy based routing rules is only available from the command line interface and WebUI.

#! REMOTE::SSH
${nice-path} -n 15 grep "routed:instance:default:pbrrules:priority" /config/active

#! PARSER::AWK


# routed:instance:default:pbrrules:priority:6:to:2.2.2.0 t
# routed:instance:default:pbrrules:priority:6:to:2.2.2.0:masklen 24
/routed:instance:default:pbrrules:priority:[0-9]+:to/ {
	split($1,splitArr,":")
	
	if(arraylen(splitArr) == 9) {
		priority = splitArr[6]

		rules[priority, "destination"] = splitArr[8] "/" $2
	}
}

# routed:instance:default:pbrrules:priority:6:from:2.3.3.0 t
# routed:instance:default:pbrrules:priority:6:from:2.3.3.0:masklen 24
/routed:instance:default:pbrrules:priority:[0-9]+:from/ {
	split($1,splitArr,":")
	
	if(arraylen(splitArr) == 9) {
		priority = splitArr[6]

		rules[priority, "source"] = splitArr[8] "/" $2
	}
}

# routed:instance:default:pbrrules:priority:6:protocol 6
/routed:instance:default:pbrrules:priority:[0-9]+:protocol/ {
	split($1,splitArr,":")
	priority = splitArr[6]
	protocol = $NF
	if (protocol == "6") {
		protocol = "TCP"
	} else if (protocol == "17") {
		protocol = "UDP"
	} else if (protocol == "1") {
		protocol = "ICMP"
	}
	
	rules[priority, "protocol"] = protocol
}

# routed:instance:default:pbrrules:priority:6:port 20
/routed:instance:default:pbrrules:priority:[0-9]+:port/ {
	split($1,splitArr,":")
	priority = splitArr[6]
	
	rules[priority, "service-port"] = $NF
}

# routed:instance:default:pbrrules:priority:6:dev eth0
/routed:instance:default:pbrrules:priority:[0-9]+:port/ {
	split($1,splitArr,":")
	priority = splitArr[6]
	
	rules[priority, "interface"] = $NF
}

# routed:instance:default:pbrrules:priority:6:table 3
/routed:instance:default:pbrrules:priority:[0-9]+:table/ {
	split($1,splitArr,":")
	priority = splitArr[6]
	
	rules[priority, "table"] = $NF
}

# routed:instance:default:pbrrules:priority:6:tname routeDefault
/routed:instance:default:pbrrules:priority:[0-9]+:tname/ {
	split($1,splitArr,":")
	priority = splitArr[6]
	
	rules[priority, "table-name"] = $NF
}

END {
	writeComplexMetricObjectArray("pbr-rules", null, rules)
}

































