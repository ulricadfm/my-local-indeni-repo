#! META
name: chkp-gaia-show_route-vsx
description: run "show route" over Clish in vsx
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: checkpoint
    os.name: gaia
    vsx: true
    role-firewall: true
    asg:
        neq: true

#! COMMENTS
static-routing-table:
    why: |
        It is important that the routing is configured the same for all cluster members of the same cluster. Otherwise there can be downtime in the event of a failover.
    how: |
        By parsing the gaia configuration database, /config/active, the static routes are retrieved. It can also be retrieved via Clish, but that creates a lot of log entries in /var/log/messages.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        Listing static routes is only available from the command line interface or via SNMP. In VSX it is also visible in SmartDashboard.

connected-networks-table:
    why: |
        It is important that the connected interfaces is configured the same, for all cluster members of the same cluster. Otherwise there can be downtime in the event of a failure.
    how: |
         By parsing the gaia configuration database, /config/active, the routes for directly connected interfaces are retrieved. It can also be retrieved via Clish, but that creates a lot of log entries in /var/log/messages.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        Listing routes for directly connected interfaces is only available from the command line interface, or SNMP.

#! REMOTE::SSH
${nice-path} -n 15 vsx stat -l | grep VSID | awk '{print $NF}' | while read id; do vsenv $id && ${nice-path} -n 15 vsx stat $id && ${nice-path} -n 15 netstat -rn; done

#! PARSER::AWK


# For VSX the routes for VSes are configured in SmartDashboard and not visible in the gaia config DB, only in netstat.


# Function to calculate number of binary 1s in a decimal number
function count1s(N) {
	r = ""                    # initialize result to empty (not 0)
	while(N != 0){            # as long as number still has a value
		r = ((N%2)?"1":"0") r   # prepend the modulos2 to the result
		N = int(N/2)            # shift right (integer division by 2)
	}

	# count number of 1s
	r = gsub(/1/,"",r)
	# Return result
	return r
}

# Function to convert a subnetmask (example: 255.255.255.0) to subnet prefix (example: 24)
function subnetmaskToPrefix(subnetmask) {
	split(subnetmask, v, "\\.")
    return count1s(v[1]) + count1s(v[2]) + count1s(v[3]) + count1s(v[4])
}

function addVsTags(tags) {
	tags["vs.id"] = vsid
    tags["vs.name"] = vsname
}

function dumpVsRouteData() {
	addVsTags(vstags)

	if (arraylen(routes)) {
		writeComplexMetricObjectArrayWithLiveConfig("static-routing-table", vstags, routes, "Static Routing Table")
	}
	
	if (arraylen(directRoutes)) {
		writeComplexMetricObjectArrayWithLiveConfig("connected-networks-table", vstags, directRoutes, "Directly Connected Networks")
	}
	
	# delete arrays between VSes
	delete routes
	delete directRoutes
}

BEGIN {
	vsid = ""
	vsname = ""
}

# VSID:            0
/VSID:/ {
    if (vsid != "") {
        # write the previous VS's data
        dumpVsRouteData()
    }
	vsid = trim($NF)
}

# Name:            VSX-CXL2-Gear
/Name:/ {
	vsname = trim($NF)
}


# 10.11.2.0       0.0.0.0         255.255.255.0   U         0 0          0 eth1
/^(?:[0-9]{1,3}\.){3}[0-9]{1,3}/ {
	destination = $1
	mask = $3
	subnetprefix = subnetmaskToPrefix(mask)
	flags = $4
	gateway = $2
	
	# If its a direct connected network route
	if (gateway == "0.0.0.0") {
		idirectRoute++
		
		directRoutes[idirectRoute, "network"] = destination
		directRoutes[idirectRoute, "mask"] = subnetprefix
	}

	# If its not a directly connected network
	if (gateway != "0.0.0.0") {
		iroute++
		routes[iroute, "network"] = destination
		routes[iroute, "mask"] = subnetprefix
		routes[iroute, "next-hop"] = gateway
	}
}

END {
	# Dump the last data
	dumpVsRouteData()
}
