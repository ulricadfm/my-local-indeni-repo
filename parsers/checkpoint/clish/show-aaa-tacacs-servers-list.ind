#! META
name: chkp-clish-show_aaa_tacacs-servers_list
description: run "show aaa tacacs-servers list" over clish
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: checkpoint
    or:
        -
            os.name: gaia
        -
            os.name: ipso

#! COMMENTS
tacacs-servers:
    why: |
        If the TACACS servers are configured incorrectly, it might not be possible for an administrator to login to the device.
    how: |
        Parse the clish configuration database in /config/active and retreive the currently configured TACACS servers. It is also possible to list them using clish, but that generates a large amount of logs in /var/log/messages when done repeatedly.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing TACACS servers is only available from the command line interface and WebUI.

tacacs-enabled:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15  egrep "aaa[^:]*:auth_profile:base_tacplus_authprofile" /config/active

#! PARSER::AWK

# aaa_tacp:auth_profile:base_tacplus_authprofile:tacp_srv:1:timeout 5
# aaa:auth_profile:base_tacplus_authprofile:tacplus_srv:1:timeout 5

/aaa[^:]*:auth_profile:base_tacplus_authprofile:tacp[a-z_]*_srv:[0-9]+:timeout/ {
	split($1,splitArr,":")
	priority=splitArr[5]

	tacacs[priority, "priority"]=priority
	tacacs[priority, "timeout"]=$NF
}

# aaa_tacp:auth_profile:base_tacplus_authprofile:tacp_srv:1:host 1.1.11.1
# aaa :auth_profile:base_tacplus_authprofile:tacplus_srv:2:host 3.3.3.3
/aaa[^:]*:auth_profile:base_tacplus_authprofile:tacp[a-z_]*_srv:[0-9]+:host/ {
	split($1,splitArr,":")
	priority=splitArr[5]
	tacacs[priority, "host"]=$NF
}

# For Gaia:
# aaa_tacp:auth_profile:base_tacplus_authprofile:tacp_srv:state on
/aaa[^:]*:auth_profile:base_tacplus_authprofile:tacp_srv:state/ {
	if ($NF == "on") {
		status = "true"
	} else {
		status = "false"
	}
	writeComplexMetricString("tacacs-enabled", null, status)
}

# For IPSO:
# aaa:auth_profile:base_tacplus_authprofile t
/aaa:auth_profile:base_tacplus_authprofile \S/ {
	if ($NF == "t") {
		status = "true"
	} else {
		status = "false"
	}
	writeComplexMetricString("tacacs-enabled", null, status)
}

END {
	writeComplexMetricObjectArrayWithLiveConfig("tacacs-servers", null, tacacs, "TACACS Servers")
}
