#! META
name: vpn_ipafile_check_vsx
description: run "vpn ipafile_check $FWDIR/conf/ipassignment.conf" on all vs in VSX
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: checkpoint
    role-firewall: true
    vsx: true

#! COMMENTS
ipassignment-conf-errors:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 fw vsx stat -l | grep VSID | awk '{print $NF}' | while read id; do vsenv $id && ${nice-path} -n 15 fw vsx stat $id && (${nice-path} -n 15 vpn ipafile_check <(${nice-path} -n 15 cat $FWDIR/conf/ipassignment.conf | ${nice-path} -n 15 sed 's/^Gateway        Type.*//g' | ${nice-path} -n 15 sed 's/^=======.*//g')); done

#! PARSER::AWK

function addVsTags(tags) {
	tags["vs.id"] = vsid
	tags["vs.name"] = vsname
}

function dumpLineData() {
    addVsTags(linetags)
	writeComplexMetricObjectArray("ipassignment-conf-errors", linetags, lines)
	delete lines
	iline = 0
}

BEGIN {
	vsid = ""
	vsname = ""
}

#VSID:            0
/VSID:/ {
	if (vsid != "") {
		# write the previous VS's data
		dumpLineData()
	}

	vsid = trim($NF)
}

#Name:            lab-CP-VSXVSLS-1-R7730
/Name:/ {
	vsname = trim($NF)
}

#Invalid IP address specification in line 0058
#Line 0001 appears to be a comment because of second parameter(gateway or address)
/Line|line/ {
	iline++
	lines[iline, "line"] = $0
}

END {
	# The last VS is handled here:
	dumpLineData()
}
