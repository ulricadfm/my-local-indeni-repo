#! META
name: chkp-vsx-stat-monitoring
description: Collect VSX data
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    vsx: "true"
    role-firewall: true

#! COMMENTS
license-elements-used:
    why: |
        Reaching the limit on number of virtual systems allowed, means no new virtual systems can be created.
    how: |
        By using the Check Point built-in "vsx stat" command, the current usage and license limit is retrieved.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing the current number of installed virtual systems and the limit is available from the command line interface.

license-elements-limit:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 vsx stat -v

#! PARSER::AWK

#Number of Virtual Systems allowed by license:          25
/^Number of Virtual Systems allowed by license:/ {
	vsAllowed = $8
}

#Virtual Systems [active / configured]:                  2 / 2 
/^Virtual Systems \[active \/ configured\]:/ {
	vsConfigured = $8
	vsActive = $6
}

END {
	tags["name"] = "virtual-systems"
	writeDoubleMetric("license-elements-limit", tags, "gauge", 300, vsAllowed)
	writeDoubleMetric("license-elements-used", tags, "gauge", 300, vsActive)
}