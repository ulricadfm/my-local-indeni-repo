#! META
name: nexus-show-resource
description: Nexus Resources
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: cisco
    os.name: nxos

#! COMMENTS
system-resource-usage:
    why: |
       Collect system level resource information to monitor for resource exhaustion. An alert will be generated when the resource utilization gets close to the maximum threshold. The following resources are monitored: Number of VLANs, VRFs, Port-Channels, IPv4 and IPv4 unicast and multicast routes and Monitor (SPAN) sessions.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the resource utilization data using the "show resource" command. The output includes a complete report of the resource utilization and the minimum and maximum threshold for each one of the resources.
    without-indeni: |
       It is not possible to poll this data through SNMP. When certain resources are exhaused a syslog message will be generated. At this stage there could be some impact to the system capability to forward traffic.
    can-with-snmp: false
    can-with-syslog: true

system-resource-limit:
    skip-documentation: true

#! REMOTE::SSH
show resource | xml

#! PARSER::XML
_omit:
    _bottom: 1
_vars:
    root: //__readonly__ 
_metrics:
    -
        _groups:
            ${root}/TABLE_vdc_resource_local/ROW_vdc_resource_local:
                _value.double:
                    _text: "used"
                _tags:
                    "im.name":
                        _constant: "system-resource-usage"
                    "name":
                        _text: "res_name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "System Resources - Usage"
                    "im.dstype.displayType":
                        _constant: "number"            
                    "im.identity-tags":
                        _constant: "name"
    -
        _groups:
            ${root}/TABLE_vdc_resource_local/ROW_vdc_resource_local:
                _value.double:
                    _text: "max"
                _tags:
                    "im.name":
                        _constant: "system-resource-limit"
                    "name":
                        _text: "res_name"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "System Resources - Limit"
                    "im.dstype.displayType":
                        _constant: "number"              
                    "im.identity-tags":
                        _constant: "name"
