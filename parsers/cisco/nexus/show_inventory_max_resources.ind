#! META
name: nexus-show-inventory-max-resources
description: Nexus show inventory for max resources (routes, arp)
type: monitoring
monitoring_interval: 43200 minutes
requires:
    vendor: cisco
    os.name: nxos

#! COMMENTS
routes-limit:
    why: |
       Identify the maximum number of IPv4 unicast routes a certain Cisco Nexus platform can support. This information is used to generate reports if the total number of routes reaches the limit.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show inventory" command. The reported device type is used to identify the max routes value based on a static table.
	   Nexus 3000: 40,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus3000/sw/scalability/7x/b_Nexus3k_Verified_Scalability_7x/b_Nexus3k_Verified_Scalability_7x_chapter_01.html)
       Nexus 5000: 7,200 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/configuration_limits/limits_513/nexus_5000_config_limits_513.html)
       Nexus 5500: 7,200 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5500/sw/Verified_Scalability/700N11/b_N5500_Verified_Scalability_700N11/b_N5500_Verified_Scalability_700N11_chapter_01.html
       Nexus 5600: 8,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5600/sw/verified_scalability/701N11/b_N5600_Verified_Scalability_701N11/b_N6000_Verified_Scalability_700N11_chapter_01.html)
       Nexus 6000: 8,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5600/sw/verified_scalability/701N11/b_N5600_Verified_Scalability_701N11/b_N6000_Verified_Scalability_700N11_chapter_01.html)
       Nexus 7000: 56,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/6_x/nx-os/unicast/configuration/guide/b-7k-Cisco-Nexus-7000-Series-NX-OS-Unicast-Routing-Configuration-Guide-Release-6x/n7k_unicast_managinging_rib_fib.html)
       Nexus 9300: 12,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus9000/sw/7-x/scalability/guide_703I11/b_Cisco_Nexus_9000_Series_NX-OS_Verified_Scalability_Guide_703I11.html)
       Nexus 9500: 128,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus9000/sw/7-x/scalability/guide_703I11/b_Cisco_Nexus_9000_Series_NX-OS_Verified_Scalability_Guide_703I11.html)
    without-indeni: |
       This data is not available through SNMP or syslog.
    can-with-snmp: false
    can-with-syslog: false

routes-limit-ipv6:
    why: |
       Identify the maximum number of IPv6 unicast routes a certain Cisco Nexus platform can support. This information is used to generate reports if the total number of routes reaches the limit.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show inventory" command. The reported device type is used to identify the max routes value based on a static table.
	   Nexus 3000: 40,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus3000/sw/scalability/7x/b_Nexus3k_Verified_Scalability_7x/b_Nexus3k_Verified_Scalability_7x_chapter_01.html)
       Nexus 5000: 3,600 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/configuration_limits/limits_513/nexus_5000_config_limits_513.html)
       Nexus 5500: 3,600 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5500/sw/Verified_Scalability/700N11/b_N5500_Verified_Scalability_700N11/b_N5500_Verified_Scalability_700N11_chapter_01.html
       Nexus 5600: 4,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5600/sw/verified_scalability/701N11/b_N5600_Verified_Scalability_701N11/b_N6000_Verified_Scalability_700N11_chapter_01.html)
       Nexus 6000: 4,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5600/sw/verified_scalability/701N11/b_N5600_Verified_Scalability_701N11/b_N6000_Verified_Scalability_700N11_chapter_01.html)
       Nexus 7000: 32,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/6_x/nx-os/unicast/configuration/guide/b-7k-Cisco-Nexus-7000-Series-NX-OS-Unicast-Routing-Configuration-Guide-Release-6x/n7k_unicast_managinging_rib_fib.html)
       Nexus 9300: 6,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus9000/sw/7-x/scalability/guide_703I11/b_Cisco_Nexus_9000_Series_NX-OS_Verified_Scalability_Guide_703I11.html)
       Nexus 9500: 20,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus9000/sw/7-x/scalability/guide_703I11/b_Cisco_Nexus_9000_Series_NX-OS_Verified_Scalability_Guide_703I11.html)
    without-indeni: |
       This data is not available through SNMP or syslog.
    can-with-snmp: false
    can-with-syslog: false

arp-limit:
    why: |
       Identify the maximum number of IPv4 ARP entries a certain Nexus platform can support. This information is used to generate reports if the total number of entries reaches the limit.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show inventory" command. The reported device type is used to identify the ARP cache size value based on a static table.
	   Nexus 3000: 4,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus3000/sw/scalability/7x/b_Nexus3k_Verified_Scalability_7x/b_Nexus3k_Verified_Scalability_7x_chapter_01.html)
       Nexus 5000: 8,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/configuration_limits/limits_513/nexus_5000_config_limits_513.html)
       Nexus 5500: 8,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5500/sw/Verified_Scalability/700N11/b_N5500_Verified_Scalability_700N11/b_N5500_Verified_Scalability_700N11_chapter_01.html
       Nexus 5600: 32,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5600/sw/verified_scalability/701N11/b_N5600_Verified_Scalability_701N11/b_N6000_Verified_Scalability_700N11_chapter_01.html)
       Nexus 6000: 32,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5600/sw/verified_scalability/701N11/b_N5600_Verified_Scalability_701N11/b_N6000_Verified_Scalability_700N11_chapter_01.html)
       Nexus 7000: 60,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/6_x/nx-os/unicast/configuration/guide/b-7k-Cisco-Nexus-7000-Series-NX-OS-Unicast-Routing-Configuration-Guide-Release-6x/n7k_unicast_managinging_rib_fib.html)
       Nexus 9300: 5,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus9000/sw/7-x/scalability/guide_703I11/b_Cisco_Nexus_9000_Series_NX-OS_Verified_Scalability_Guide_703I11.html)
       Nexus 9500: 40,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus9000/sw/7-x/scalability/guide_703I11/b_Cisco_Nexus_9000_Series_NX-OS_Verified_Scalability_Guide_703I11.html)
    without-indeni: |
       This data is not available through SNMP or syslog.
    can-with-snmp: false
    can-with-syslog: false

neighbor-discovery-limit:
    why: |
       Identify the maximum number of IPv6 neighbor entries a certain Nexus platform can support. This information is used to generate reports if the total number of entries reaches the limit.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show inventory" command. The reported device type is used to identify the maximum size of the neighbor discovery cache based on a static table.
	   Nexus 3000: 1,500 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus3000/sw/scalability/7x/b_Nexus3k_Verified_Scalability_7x/b_Nexus3k_Verified_Scalability_7x_chapter_01.html)
       Nexus 5000: 4,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/configuration_limits/limits_513/nexus_5000_config_limits_513.html)
       Nexus 5500: 4,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5500/sw/Verified_Scalability/700N11/b_N5500_Verified_Scalability_700N11/b_N5500_Verified_Scalability_700N11_chapter_01.html
       Nexus 5600: 16,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5600/sw/verified_scalability/701N11/b_N5600_Verified_Scalability_701N11/b_N6000_Verified_Scalability_700N11_chapter_01.html)
       Nexus 6000: 16,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5600/sw/verified_scalability/701N11/b_N5600_Verified_Scalability_701N11/b_N6000_Verified_Scalability_700N11_chapter_01.html)
       Nexus 7000: 30,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/6_x/nx-os/unicast/configuration/guide/b-7k-Cisco-Nexus-7000-Series-NX-OS-Unicast-Routing-Configuration-Guide-Release-6x/n7k_unicast_managinging_rib_fib.html)
       Nexus 9300: 5,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus9000/sw/7-x/scalability/guide_703I11/b_Cisco_Nexus_9000_Series_NX-OS_Verified_Scalability_Guide_703I11.html)
       Nexus 9500: 30,000 (http://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus9000/sw/7-x/scalability/guide_703I11/b_Cisco_Nexus_9000_Series_NX-OS_Verified_Scalability_Guide_703I11.html)
    without-indeni: |
       This data is not available through SNMP or syslog.
    can-with-snmp: false
    can-with-syslog: false


#! REMOTE::SSH
show inventory | include Chassis

#! PARSER::AWK

BEGIN {
    max_ipv4 = 0
    max_ipv6 = 0
    max_arp = 0
    max_v6_nd = 0
}

/DESCR:.*Nexus.*3000/ {
    max_ipv4 = 40000
    max_ipv6 = 40000
    max_arp = 4000
    max_v6_nd = 1500
}

/DESCR:.*Nexus.*50/ {
    max_ipv4 = 7200
    max_ipv6 = 3600
    max_arp = 8000
    max_v6_nd = 4000
}

/DESCR:.*Nexus.*55/ {
    max_ipv4 = 7200
    max_ipv6 = 3600
    max_arp = 8000
    max_v6_nd = 4000
}

/DESCR:.*Nexus.*56/ {
    max_ipv4 = 8000
    max_ipv6 = 4000
    max_arp = 32000
    max_v6_nd = 16000
}

# Nexus 6000 registers as Nexus 5600

/DESCR:.*Nexus.*7/ {
    max_ipv4 = 56000
    max_ipv6 = 32000
    max_arp = 60000
    max_v6_nd = 30000
}

/DESCR:.*Nexus9000 C93/ {
    max_ipv4 = 12000
    max_ipv6 = 6000
    max_arp = 5000
    max_v6_nd = 5000
}

/DESCR:.*Nexus9000 C95/ {
    max_ipv4 = 128000
    max_ipv6 = 20000
    max_arp = 40000
    max_v6_nd = 30000
}

END {
    if (max_ipv4 > 0) {
        writeDoubleMetric("routes-limit", null, "gauge", 2592000, max_ipv4)
    }
    if (max_ipv6 > 0) {
        writeDoubleMetric("routes-limit-ipv6", null, "gauge", 2592000, max_ipv4)
    }
    if (max_arp > 0) {
        writeDoubleMetric("arp-limit", null, "gauge", 2592000, max_arp)
    }
    if (max_v6_nd > 0) {
        writeDoubleMetric("neighbor-discovery-limit", null, "gauge", 2592000, max_v6_nd)
    }
}
