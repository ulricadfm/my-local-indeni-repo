#! META
name: freebsd-swapinfo
description: fetch swap memory utilization
type: monitoring
monitoring_interval: 5 minutes
requires:
    freebsd-based: "true"

#! COMMENTS
memory-free-kbytes:
    skip-documentation: true

memory-total-kbytes:
    skip-documentation: true

memory-usage:
    why: |
        If memory is low, this could have impact on services and performance.
    how: |
        By running the built-in "swapinfo" command, memory information is retreived for Swap.
    without-indeni: |
        An administrator could login and manually check the memory allocation, or via SNMP.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or vendor-provided tools.

#! REMOTE::SSH
${nice-path} -n 15 swapinfo

#! PARSER::AWK


# Device          1K-blocks     Used    Avail Capacity
/Device/ {
	getColumns(trim($0), "[ \t]+", columns)	
}

# /dev/mirror/gmroots1b   8388608        0  8388608     0%
/[0-9]%/ {
	swaptag["name"] = "swap"
	totalSwap = getColData(trim($0), columns, "1K-blocks")
	usedSwap = getColData(trim($0), columns, "Used")
	freeSwap = getColData(trim($0), columns, "Avail")
	
	# if swap is 0 then the calculation will fail since the result will be "NaN"
	if (totalSwap != 0) {
		usageSwap = (usedSwap / totalSwap) * 100
		writeDoubleMetricWithLiveConfig("memory-usage", swaptag, "gauge", "300", usageSwap, "Memory - Usage", "kbytes", "name")
		writeDoubleMetricWithLiveConfig("memory-free-kbytes", swaptag, "gauge", "300", freeSwap, "Memory - Free", "kbytes", "name")
		writeDoubleMetricWithLiveConfig("memory-total-kbytes", swaptag, "gauge", "300", totalSwap, "Memory - Total", "kbytes", "name")
	}
}
