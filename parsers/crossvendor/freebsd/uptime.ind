#! META
name: freebsd-uptime
description: fetch uptime in seconds
type: monitoring
monitoring_interval: 5 minutes
requires:
    freebsd-based: true

#! COMMENTS
uptime-seconds:
    why: |
        If uptime is suddently reduced, this means the device has rebooted.
    how: |
        The current uptime is retreived by calculating the difference between the current time and the kernel boot time.
    without-indeni: |
        An administrator could login and manually check the uptime.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface, SNMP or vendor-provided management interface.

#! REMOTE::SSH
echo -n "curdate: " ; ${nice-path} -n 15 date +%s
echo -n "boottime: " ; ${nice-path} -n 15 sysctl kern.boottime | sed 's/.* sec = //' | sed 's/,.*//'

#! PARSER::AWK

#curdate: 1503135122
/curdate/ {
    curdate = $NF
}

#boottime: 1503077738
/boottime/ {
    boottime = $NF
}

END {
    if (curdate && boottime) {
        uptime = curdate - boottime
        writeDoubleMetricWithLiveConfig("uptime-seconds", null, "gauge", 300, uptime, "Uptime", "duration", "")
    }
}