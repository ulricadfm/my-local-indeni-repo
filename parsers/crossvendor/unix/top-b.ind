#! META
name: unix-top-b
description: run "top -b -n 1" to determine load average, zombie tasks and more
type: monitoring
monitoring_interval: 10 minutes
requires:
    or:
        -
            linux-based: "true"
        -
            freebsd-based: "true"
    os.name: 
        neq: secureplatform

#! COMMENTS
load-average-one-minute:
    why: |
        The load average is a value which describes the load of the system. The calculated load average depends on the number of cores. For a 2 CPU core system, a load average of 2 for the past 5 minutes means that the system was, on average, fully utilizing the CPU resources available.
    how: |
        Using the built in "top" command, the load average is listed for 1, 5 and 15 minutes.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing the load average is only available from the command line interface.

load-average-five-minutes:
    why: |
        The load average is a value which describe the load of the system. It is however dependant on the number of cores, so to know if the value is high or not you must also factor in this. For a 2 CPU core system, a load average of 2 for the past 5 minutes means that the system was, on average, fully utilizing the CPU resources available.
    how: |
        Using the built in "top" command, the load average is listed for 1, 5 and 15 minutes.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing the load average is only available from the command line interface.
        
load-average-fifteen-minutes:
    why: |
        The load average is a value which describe the load of the system. It is however dependant on the number of cores, so to know if the value is high or not you must also factor in this. For a 2 CPU core system, a load average of 2 for the past 5 minutes means that the system was, on average, fully utilizing the CPU resources available.
    how: |
        Using the built in "top" command, the load average is listed for 1, 5 and 15 minutes.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing the load average is only available from the command line interface.

tasks-zombies:
    why: |
        A zombie process is a child process that has died. The parent process has however not read the exit status and ended the child process. This means that it continues to use memory. Zombie processes are often a result of poorly written software. Since they still use memory a lot of zombie processes could cause issues.
    how: |
        Using the built in "top" command, the number of zombie processes is retreived.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing amount of zombie processes is only available from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 top -b -n 1 | egrep "(days|average|zombie)"

#! PARSER::AWK

# load average: 2.43, 3.16, 3.12
# last pid:  3812;  load averages:  1.23,  2.34,  3.45  up 1+01:19:04    11:54:12
/load average/ {
    line = $0
    sub(/.*load average[s]*:\s+/, "", line)
    split(line, loads, ",")
    writeDoubleMetricWithLiveConfig("load-average-one-minute", null, "gauge", "600", trim(loads[1]), "Load Average (1 Minute)", "number", "")
    writeDoubleMetricWithLiveConfig("load-average-five-minutes", null, "gauge", "600", trim(loads[2]), "Load Average (5 Minutes)", "number", "")

    # the third one might have a "tail" of data, if it's in FreeBSD
    loads[3] = trim(loads[3])
    sub(/\s+.*/, "", loads[3])
    writeDoubleMetricWithLiveConfig("load-average-fifteen-minutes", null, "gauge", "600", trim(loads[3]), "Load Average (15 Minutes)", "number", "")
}

#Tasks: 217 total,   3 running, 214 sleeping,   0 stopped,   17 zombie
/^Tasks:/ {
	zombies=$(NF-1)
	writeDoubleMetric("tasks-zombies", null, "gauge", "600", zombies)
}

