#! META
name: unix-date-current
description: Gives current time in unix time
type: monitoring
monitoring_interval: 5 minutes
requires:
    or:
        -
            linux-based: true
        -
            linux-busybox: true
        -
            freebsd-based: true

#! COMMENTS
current-datetime:
    why: |
        The current time of the device is collected. If the time is incorrect this also means timestamps in logs are incorrect, and could also affect certificates and other methods relying on the time being set correctly. It is also important that members of the same cluster have their clocks in sync.
    how: |
        Using the built in "date" command.
    without-indeni: |
        An administrator could login and manually check the time.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface or via SNMP.

#! REMOTE::SSH
${nice-path} -n 15 date +%s

#! PARSER::AWK

# 1471076153
/[0-9]+/ {
	writeDoubleMetricWithLiveConfig("current-datetime", null, "gauge", 300, $1, "Current Date/Time", "date", "")
}
