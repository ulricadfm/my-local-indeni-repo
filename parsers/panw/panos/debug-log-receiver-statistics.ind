#! META
name: panos-debug-log-receiver-statistics
description: grab debug stats of log-receiver
type: monitoring
monitoring_interval: 30 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos

#! COMMENTS
logs-discarded-queue-full:
    why: |
        Traffic logs are sent from the firewalls to Panorama and log servers. Sometimes there are issues with the flow of such logs (like https://live.paloaltonetworks.com/t5/Management-Articles/Traffic-Log-is-Not-Generated-and-Not-Displayed-on-the-WebGUI/ta-p/62177). In order to identify such issues promptly, it is important to track the log-forward discards.
    how: |
        This script logs into the Palo Alto Networks firewall through SSH and retrieves the log receiver statistics. It focuses on the discards due to the queue being full or send errors.
    without-indeni: |
        An administrator would need to write a script to poll their firewalls for the data. Alternatively one could retrieve the data once an issue occurs.
    can-with-snmp: false
    can-with-syslog: false
log-forward-discarded-send-error:
    skip-documentation: true
log-forward-discarded-queue-full:
    skip-documentation: true

#! REMOTE::SSH
debug log-receiver statistics

#! PARSER::AWK
BEGIN {
}

/Logs discarded/ {
	writeDoubleMetric("logs-discarded-queue-full", null,"counter",1800,$NF)
}
/Log Forward discarded.*queue full/ {
	writeDoubleMetric("log-forward-discarded-queue-full", null,"counter",1800,$NF)
}
/Log Forward discarded.*send error/ {
	writeDoubleMetric("log-forward-discarded-send-error", null,"counter",1800,$NF)
}
END {
}
