#! META
name: panos-show-session-info
description: fetch the status of sessions
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: paloaltonetworks
    os.name: panos
    product: firewall

#! COMMENTS
concurrent-connections-limit:
    why: |
        Each device has a limit to the number of concurrent sessions or connections it supports. Across the Palo Alto Networks product line, different devices are sized for different amounts of sessoins. Reaching the maximum number of sessions allowed may result in an outage.
    how: |
        This alert uses the Palo Alto Networks API to retrieve the current utilization of the number of sessions allowed in total.
    without-indeni: |
        To know what is the current utilization of sessions on a Palo Alto Networks firewall, the user will normally use the CLI.
    can-with-snmp: true
    can-with-syslog: true
concurrent-connections:
    why: |
        Tracking the number of concurrent connections is helpful in understanding traffic patterns and load.
    how: |
        This alert uses the Palo Alto Networks API to retrieve the current utilization of the number of sessions allowed in total.
    without-indeni: |
        To know what is the current utilization of sessions on a Palo Alto Networks firewall, the user will normally use the CLI. A script could be written to track this data on an ongoing basis.
    can-with-snmp: true
    can-with-syslog: true
connections-per-second:
    skip-documentation: true

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><session><info><%2Finfo><%2Fsession><%2Fshow>&key=${api-key}
protocol: HTTPS

#! PARSER::XML
_vars:
    root: /response/result
_metrics:
    -
        _value.double:
            _text: ${root}/num-active
        _tags:
            "im.name":
                _constant: "concurrent-connections"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "Sessions - Current"
            "im.dstype.displayType":
                _constant: "number"
    -
        _value.double:
            _text: ${root}/num-max
        _tags:
            "im.name":
                _constant: "concurrent-connections-limit"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "Sessions - Limit"
            "im.dstype.displayType":
                _constant: "number"
    -
        _value.double:
            _text: ${root}/cps
        _tags:
            "im.name":
                _constant: "connections-per-second"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "CPS"
            "im.dstype.displayType":
                _constant: "number"
