#! META
name: junos-show-configuration-system-syslog
description: JUNOS SRX retrieve syslog server configuration information 
type: monitoring
monitoring_interval: 60 minute
requires:
    vendor: juniper
    os.name: junos
    product: firewall

#! COMMENTS
syslog-servers:
    why: |
        The SRX device can send log messages to the remote syslog servers. 
    how: |
        This script retrieves how the syslog servers are configured on the SRX device by running the command "show configuration system syslog" via SSH connection to a device. 
    without-indeni: |
        An administrator could log on to the device to run the command "show configuration system syslog" to collect the same information.
    can-with-snmp: false 
    can-with-syslog: false
    vendor-provided-management: |
        The commamnd line is available to retrieve this information

#! REMOTE::SSH
show configuration system syslog | display set | match host

#! PARSER::AWK
#set system syslog host 192.168.1.56 any critical
/^(set\s+system\s+syslog\s+host)/{
    host = $(NF - 2)
    facility = $(NF-1)
    severity = $NF 
    syslog_server[idx_1,"ipaddress"] = host 
    syslog_server[idx_1,"severity"] = severity 
    idx_1++
}

END{
    writeComplexMetricObjectArray("syslog-servers", null, syslog_server)
}
