#! META
name: junos-show-security-ipsec
description: JUNOS get VPN/ipsec information
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: juniper
    os.name: junos
    product: firewall

#! COMMENTS
vpn-tunnel-state:
    why: |
        The IPSec VPN state can indicate whether the IPSec VPN has been correctly configured and whether the VPN is up or down.
    how: |
        The script runs "show configuration security ike, show configuration security ipsec, show security ipsec inactive-tunnels, show security ipsec security-associations brief" to retrieve IPSec VPN related information.
    without-indeni: |
        An administrator can run "show configuration security ike, show configuration security ipsec, show security ipsec inactive-tunnels, show security ipsec security-associations brief" via SSH connection to retrieve the same inforamtion.

    can-with-snmp: partial true
    can-with-syslog: partial true
    vendor-provided-management: |
        The command line can provide the same information

#! REMOTE::SSH
show configuration security ike
show configuration security ipsec
show security ipsec inactive-tunnels 
show security ipsec security-associations brief 

#! PARSER::AWK

BEGIN {
    gatewayCol = 0
}

#####
# We first get the IKE gateways - so we can translate gateway name to IP
#####

#gateway srx220 {
/^gateway .* \{$/ {
    ikeGatewayName = $2
    inIkeGateway = "true"
}

#    address 192.168.1.20;
/^\s+address [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3};/ {
    ipAddress = $2
    sub(/;/, "", ipAddress)

    if (inIkeGateway == "true") {
        gateways[ikeGatewayName] = ipAddress
    }
}

#}
/^}$/ {
    inIkeGateway = "false"
}

######
# Parsing of ipsec, matching to IKE data.
######

#vpn ipsec-vpn-cfgr {
/vpn .* \{/ {
    inVpnIpsec = "true"
}

#vpn-monitor-options
/\s+vpn-monitor/ {
    hasVpnMonitor = "true"
}

#        gateway ike-gate-cfgr;
/\s+gateway \S+;/ {
    ikeGatewayRef = $2
    sub(/;/, "", ikeGatewayRef)

    if (gateways[ikeGatewayRef]) {
        currentIkeIp = gateways[ikeGatewayRef]
    }
}

#}
/^}$/ {
    if (inVpnIpsec == "true") {
        if (currentIkeIp != "" && hasVpnMonitor == "true") {
            monitoredGatways[currentIkeIp] = "0"
        }
        currentIkeIp = ""
        hasVpnMonitor = "false"
        inVpnIpsec = "false"
    }
}

#####
# Parsing actual tunnel status
#####

#  Total inactive tunnels: 1
/Total inactive tunnels/ {
    state = 0    
}

#Total active tunnels: 2
/Total active tunnels/ {
    state = 1
}

#ID Gateway Port Algorithm SPI Life:sec/kb Mon vsys
/ID.*Gateway/ {
    getColumns(trim($0), "[ \t]+", columns)
    gatewayCol = getColId(columns, "Gateway")
}

#<16384 1.1.1.1 500 ESP:3des/sha1 af88baa 28795/unlim D 0
#>16384 1.1.1.1 500 ESP:3des/sha1 f4e3e5f4 28795/unlim D 0
/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/ {
    peer = $gatewayCol
    vpntags["name"] = "gateway " peer
    vpntags["peerip"] = peer

    if (monitoredGatways[peer] ~ /[0-1]/) {
        vpntags["always-on"] = "true"

        if (state == 1) {
            monitoredGateways[peer] = "1"
        }
    } else {
        vpntags["always-on"] = "false"
    }

    writeDoubleMetricWithLiveConfig("vpn-tunnel-state", vpntags, "gauge", 300, state, "VPN Tunnels - State", "state", "name")
}

END { 
    # Dump all the monitored gateways state, just in case
    for (peer in monitoredGateways) {
        state = monitoredGateways[peer]
        vpntags["name"] = "gateway " peer
        vpntags["peerip"] = peer
        vpntags["always-on"] = "true"

        writeDoubleMetricWithLiveConfig("vpn-tunnel-state", vpntags, "gauge", 300, state, "VPN Tunnels - State", "state", "name")
    }
}

